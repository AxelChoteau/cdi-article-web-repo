<%@ page
	language="java"
	contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<nav class="navbar navbar-expand-md navbar-dark blue-gradient">
	<div class="container">
		<a
			class="navbar-brand"
			href="/cdi-articles-web"><i class="fas fa-shopping-cart mr-2"></i>Stock
			Options</a>
		<button
			class="navbar-toggler"
			type="button"
			data-toggle="collapse"
			data-target="#basicExampleNav"
			aria-controls="basicExampleNav"
			aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<!-- Collapsible content -->
		<div
			class="collapse navbar-collapse"
			id="basicExampleNav">
			<!-- Links -->
			<ul class="navbar-nav mr-auto">
				<c:if test="${ !empty sessionScope.userSession }">
					<li class="nav-item"><a
						class="nav-link"
						href="<c:url value="/articles"/>">Articles</a></li>
					<c:if test="${ sessionScope.userSession.role != 'USER' }">
						<li class="nav-item"><a
							class="nav-link"
							href="<c:url value="/users"/>">Users</a></li>
					</c:if>
				</c:if>
			</ul>
			<!-- Links -->
			<c:choose>
				<c:when test="${ !empty sessionScope.userSession }">
					<div class="nav-item">
						<a
							class="nav-link text-white"
							href="/cdi-articles-web/signout">Log out<i
							class="fas fa-sign-out-alt"></i></a>
					</div>
				</c:when>
				<c:otherwise>
					<div class="nav-item">
						<a
							class="nav-link text-white"
							href="/cdi-articles-web/connection">Log in<i
							class="fas fa-sign-in-alt"></i></a>
					</div>
				</c:otherwise>
			</c:choose>
		</div>
		<!-- Collapsible content -->
	</div>
</nav>