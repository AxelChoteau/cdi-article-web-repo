<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<input type="number" id="articleId" name="articleId" value="${ article.id }" hidden="hidden">
<div class="md-form">
	<input type="text" id="articleLabel" name="articleLabel"
		class="form-control <c:choose>
    <c:when test="${form.errors['articleLabel']==null}">is-valid</c:when>
    <c:otherwise>is-invalid</c:otherwise>
</c:choose>"
		value="<c:out value="${article.label}"/>"> <label
		for="articleLabel">Label</label> <span id=articleLabelError
		class="invalid-feedback"> <c:out
			value="${form.errors['articleLabel']}" />
	</span>
</div>
<div class="md-form">
	<textarea id="articleDescription" name="articleDescription"
		class="md-textarea form-control <c:choose>
    <c:when test="${form.errors['articleDescription']==null}">is-valid</c:when>
    <c:otherwise>is-invalid</c:otherwise>
</c:choose>"><c:out
			value="${article.description}" /></textarea>
	<label for="articleDescription">Description</label> <span
		id=articleDescriptionError class="invalid-feedback"> <c:out
			value="${form.errors['articleDescription']}" />
	</span>
</div>
<div class="md-form">
	<input type="number" id="articlePrice" name="articlePrice" min="0"
		class="form-control <c:choose>
    <c:when test="${form.errors['articlePrice']==null}">is-valid</c:when>
    <c:otherwise>is-invalid</c:otherwise>
</c:choose>"
		value="<c:out value="${article.price}"/>"> <label
		for="articlePrice">Price</label> <span id=articlePriceError
		class="invalid-feedback"> <c:out
			value="${form.errors['articlePrice']}" />
	</span>
</div>
<div class="md-form">
	<input type="number" id="articleStock" name="articleStock" min="0"
		class="form-control <c:choose>
    <c:when test="${form.errors['articleStock']==null}">is-valid</c:when>
    <c:otherwise>is-invalid</c:otherwise>
</c:choose>"
		value="<c:out value="${article.stock}"/>"> <label
		for="articleStock">Stock</label> <span id=articleStockError
		class="invalid-feedback"> <c:out
			value="${form.errors['articleStock']}" />
	</span>
</div>
<div class="md-form">
	<label class="mt-1" for="articleImage">Image</label><br>
	<div class="file-upload btn btn-rounded btn-sm btn-add">
		<span>BROWSE</span> <input type="file" name="articleImage"
			id="articleImage"
			class="upload <c:choose>
    <c:when test="${form.errors['articleImage']==null}">is-valid</c:when>
    <c:otherwise>is-invalid</c:otherwise>
</c:choose>" />
	</div>
	<input type="text" id="fileuploadurl" readonly
		placeholder="Maximum image size is 2Mo" value="${ article.image }"> <span
		id=articleImageError class="invalid-feedback">
		<br><c:out value="${form.errors['articleImage']}" /> </span>
</div>