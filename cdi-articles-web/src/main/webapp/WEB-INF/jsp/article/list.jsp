<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>CDI Axel Choteau</title>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.6.1/css/all.css">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/mdb.min.css">
<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<div class="full-size">
		<c:import url="../inc/header.jsp" />
		<div class="container py-5">
			<div class="row">
				<div class="col-md-12">
					<c:choose>
						<c:when test="${ empty articles.content }">
							<span>There are no article saved.</span>
						</c:when>
						<c:otherwise>
							<table id="listArticles"
								class="table table-hover table-responsive-sm table-borderless">
								<thead class="thead">
									<tr role="row">
										<th></th>
										<th class="font-weight-bold">Label</th>
										<th class="font-weight-bold">Price</th>
										<th class="font-weight-bold">Stock</th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="articleItem" items="${ articles.content }">
										<tr>
											<th scope="row" class="px-3" style="width: 200px"><img
												src="<c:url value = "articles/details/images/${ articleItem.image }" />"
												alt="" class="img-fluid z-depth-0" style="width: 150px"></th>
											<td class="my-auto" style="vertical-align: middle"><c:out
													value="${ articleItem.label }" /></td>
											<td class="my-auto" style="vertical-align: middle">$<c:out
													value="${ articleItem.price }" /></td>
											<td class="my-auto" style="vertical-align: middle"><c:out
													value="${ articleItem.stock }" /></td>
											<td class="my-auto" style="vertical-align: middle"><a
												href="<c:url value="articles/details"><c:param name="id" value="${ articleItem.id }"/></c:url>">
													<i class="fas fa-arrow-right fa-lg"></i>
											</a></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</c:otherwise>
					</c:choose>
					<c:if test="${ sessionScope.userSession.role != 'USER' }">
						<a href="articles/create"><i
							class="fab py-auto material-icons">add</i></a>
					</c:if>
				</div>
			</div>
			<c:if test="${ !empty articles.content }">
				<div class="row justify-content-center">
					<div class="col-sm-12 col-md-5 py-3">
						<div class="dataTables_info">
							<c:set var="start"
								value="${ articles.number * articles.size + 1 }"></c:set>
							<c:out
								value="Showing ${ start } to ${ start + articles.getNumberOfElements()-1 } of ${ articles.getTotalElements() } entries"></c:out>
						</div>
					</div>
					<div class="col-sm-12 col-md-7 py-3 ">
						<div class="dataTables_paginate paging_simple_numbers ">
							<ul
								class="pagination pagination-circle justify-content-md-end pg-red">
								<li
									class="paginate_button page-item previous <c:if test="${ !articles.hasPrevious() }">disabled</c:if>"
									id="dtBasicExample_previous"><a
									href="<c:url value="/articles"><c:param name="page" value="${ articles.number - 1 }"/></c:url>"
									class="page-link">Previous</a></li>
								<c:forEach var="i" begin="0" end="${ articles.totalPages - 1 }">
									<c:choose>
										<c:when test="${ articles.number == i }">
											<li class="paginate_button page-item active"><a
												href="<c:url value="/articles"><c:param name="page" value="${ i }"/></c:url>"
												class="page-link"><c:out value="${ i + 1 }"></c:out></a></li>
										</c:when>
										<c:otherwise>
											<li class="paginate_button page-item"><a
												href="<c:url value="/articles"><c:param name="page" value="${ i }"/></c:url>"
												class="page-link"><c:out value="${ i + 1 }"></c:out></a></li>
										</c:otherwise>
									</c:choose>
								</c:forEach>
								<li
									class="paginate_button page-item next <c:if test="${ !articles.hasNext() }">disabled</c:if>"
									id="dtBasicExample_next"><a
									href="<c:url value="/articles"><c:param name="page" value="${ articles.number + 1 }"/></c:url>"
									class="page-link">Next</a></li>
							</ul>
						</div>
					</div>
				</div>
			</c:if>
		</div>
	</div>
	<script src="js/jquery-3.3.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/mdb.min.js"></script>
</body>
</html>