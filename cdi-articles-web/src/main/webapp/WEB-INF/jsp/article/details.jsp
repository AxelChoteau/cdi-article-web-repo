<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>CDI Axel Choteau</title>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.6.1/css/all.css">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons">
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/mdb.min.css">
<link rel="stylesheet" href="../css/style.css">
</head>
<body>
	<div class="full-size">
		<c:import url="../inc/header.jsp" />
		<div class="container my-5">
			<c:if
				test="${ userSession.role == 'ADMIN' or userSession.role == 'SUPER_ADMIN' }">
				<div class="row">
					<div class="col-12 text-right">
						<div>
							<a class="mx-2 my-3"
								href="<c:url value="/articles/details/edit"><c:param name="id" value="${ article.id }"/></c:url>"><i
								class="fas fa-pen fa-lg black-text"></i></a> <a class="mx-2 my-3"
								href="<c:url value="/articles/details/delete"><c:param name="id" value="${ article.id }"/></c:url>"><i
								class="fas fa-trash fa-lg black-text"></i></a>
						</div>
					</div>
				</div>
			</c:if>
			<div class="row">
				<div class="col-md-12 col-lg-5 text-center">
					<img
						src="<c:url value="/articles/details/images/${ article.image }"/>"
						class="img-fluid z-depth-0" style="width: 500px">
				</div>
				<div class="col-md-12 col-lg-7 my-auto text-sm-center text-lg-left">
					<h1>
						<c:out value="${ article.label }"></c:out>
					</h1>
					<h3>
						$
						<c:out value="${ article.price }"></c:out>
					</h3>
					<p class="text-justify">
						<c:out value="${ article.description }"></c:out>
					</p>
					<p class="text-right">
						<c:out value="${ article.stock }"></c:out>
						item(s) left
					</p>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="../js/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="../js/popper.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/mdb.min.js"></script>
</body>
</html>