<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>CDI Axel Choteau</title>
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/mdb.min.css">
<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<div class="vertical-center">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-sm-12 col-md-6">
					<section class="form-simple">
						<!--Form with header-->
						<div class="card">
							<!--Header-->
							<div class="header pt-3 grey darken-1">
								<div class="row d-flex justify-content-start">
									<h3 class="text-white mt-3 mb-4 pb-1 mx-5">Log in</h3>
								</div>
							</div>
							<!--Header-->
							<div class="card-body mx-4 mt-4">
								<!--Body-->
								<form method="post" action="login" novalidate>
									<div class="md-form">
										<i class="material-icons prefix">alternate_email</i> <input
											type="email" id="userEmail" name="userEmail"
											class="form-control" value="<c:out value="${ user.email }"/>">
										<label for="userEmail">Your email</label>
									</div>
									<div class="md-form pb-3">
										<i class="material-icons prefix">lock</i> <input
											type="password" id="userPassword" name="userPassword"
											class="form-control"> <label for="userPassword">Your
											password</label>
										<div class="mt-3">
											<span class="error"><c:out value="${ message }" />
											</span>
										</div>
									</div>
									<div class="text-center mb-4">
										<button type="submit"
											class="btn btn-rounded btn-outline-red btn-block">Log
											in</button>
									</div>
								</form>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
	</div>
	<script src="js/jquery-3.3.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/mdb.min.js"></script>
</body>
</html>