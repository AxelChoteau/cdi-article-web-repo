<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>CDI Axel Choteau</title>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.6.1/css/all.css">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/mdb.min.css">
<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<div class="full-size">
		<c:import url="../inc/header.jsp" />
		<div class="container py-5">
			<div class="row">
				<div class="col-md-12">
					<c:choose>
						<c:when test="${ empty users }">
							<span>There are no user saved.</span>
						</c:when>
						<c:otherwise>
							<table id="listUser" class="table table-striped table-responsive-sm table-borderless">
								<thead class="thead">
									<tr role="row">
										<th class="font-weight-bold">#</th>
										<th class="font-weight-bold">Name</th>
										<th class="font-weight-bold">Firstname</th>
										<th class="font-weight-bold">E-mail</th>
										<th class="font-weight-bold">Role</th>
										<th></th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="userItem" items="${ users.content }">
										<tr>
											<th scope="row"><c:out value="${ userItem.id }" /></th>
											<td><c:out value="${ userItem.name }" /></td>
											<td><c:out value="${ userItem.firstname }" /></td>
											<td><c:out value="${ userItem.email }" /></td>
											<td><c:out value="${ userItem.role }" /></td>
											<c:choose>
												<c:when
													test="${ userItem.role == 'USER' or (userSession.role == 'SUPER_ADMIN' and userItem.role == 'ADMIN')}">
													<td><a class="mx-2 my-3"
														href="<c:url value="users/edit"><c:param name="userId" value="${ userItem.id }" /></c:url>"><i
															class="fas fa-pen fa-lg black-text"></i></a></td>
													<td><a class="mx-2 my-3"
														href="<c:url value="users/delete"><c:param name="userId" value="${ userItem.id }" /></c:url>"><i
															class="fas fa-trash fa-lg black-text"></i></a></td>
												</c:when>
												<c:otherwise>
													<td></td>
													<td></td>
												</c:otherwise>
											</c:choose>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</c:otherwise>
					</c:choose>
					<a href="users/create"><i class="fab py-auto material-icons">add</i></a>
				</div>
			</div>
			<div class="row justify-content-center" >
				<div class="col-sm-12 col-md-5 py-3">
					<div class="dataTables_info">
						<c:set var="start" value="${ users.number * users.size + 1 }"></c:set>
						<c:out
							value="Showing ${ start } to ${ start + users.getNumberOfElements()-1 } of ${ users.getTotalElements() } entries"></c:out>
					</div>
				</div>
				<div class="col-sm-12 col-md-7 py-3 ">
					<div class="dataTables_paginate paging_simple_numbers ">
						<ul class="pagination pagination-circle justify-content-md-end pg-red">
							<li
								class="paginate_button page-item previous <c:if test="${ !users.hasPrevious() }">disabled</c:if>"
								id="dtBasicExample_previous"><a
								href="<c:url value="/users"><c:param name="page" value="${ users.number - 1 }"/></c:url>"
								class="page-link">Previous</a></li>
							<c:forEach var="i" begin="0" end="${ users.totalPages - 1 }">
								<c:choose>
									<c:when test="${ users.number == i }">
										<li class="paginate_button page-item active"><a
											href="<c:url value="/users"><c:param name="page" value="${ i }"/></c:url>"
											class="page-link"><c:out value="${ i + 1 }"></c:out></a></li>
									</c:when>
									<c:otherwise>
										<li class="paginate_button page-item"><a
											href="<c:url value="/users"><c:param name="page" value="${ i }"/></c:url>"
											class="page-link"><c:out value="${ i + 1 }"></c:out></a></li>
									</c:otherwise>
								</c:choose>
							</c:forEach>
							<li
								class="paginate_button page-item next <c:if test="${ !users.hasNext() }">disabled</c:if>"
								id="dtBasicExample_next"><a
								href="<c:url value="/users"><c:param name="page" value="${ users.number + 1 }"/></c:url>"
								class="page-link">Next</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="js/jquery-3.3.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/mdb.min.js"></script>
</body>
</html>