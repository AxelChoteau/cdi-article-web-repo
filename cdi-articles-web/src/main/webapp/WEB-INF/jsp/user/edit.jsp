<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>CDI Axel Choteau</title>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.6.1/css/all.css">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons">
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/mdb.min.css">
<link rel="stylesheet" href="../css/style.css">
</head>
<body>
	<div class="full-size">
		<c:import url="../inc/header.jsp" />
		<div class="container py-5">
			<div class="row justify-content-center">
				<div class="col-sm-12 col-md-6">
					<section class="form-simple">
						<!--Form with header-->
						<div class="card">
							<!--Header-->
							<div class="header pt-3 grey darken-1">
								<div class="row d-flex justify-content-start">
									<h3 class="text-white mt-3 mb-4 pb-1 mx-5"><i class="material-icons mr-3">person_add</i>Edit user</h3>
								</div>
							</div>
							<!--Header-->
							<div class="card-body mx-4 mt-4">
								<!--Body-->
								<form id="create_user_form" method="post" action="edit"
									novalidate>
									<div class=pb-3>
										<c:import url="userEditForm.jsp" />
									</div>
									<!--Submit -->
									<div class="text-center mb-4">
										<button type="submit"
											class="btn btn-rounded btn-outline-red btn-block">Edit</button>
									</div>
								</form>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="../js/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="../js/popper.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/mdb.min.js"></script>
	<script type="text/javascript" src="../js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="../js/additional-methods.min.js"></script>
	<script type="text/javascript" src="../js/scriptUserForm.js"></script>

</body>
</html>