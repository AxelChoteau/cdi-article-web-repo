<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="md-form">
	<input type="email" id="userEmail" name="userEmail"
		class="form-control <c:choose>
    <c:when test="${form.errors['userEmail']==null}">is-valid</c:when>
    <c:otherwise>is-invalid</c:otherwise>
</c:choose>"
		value="<c:out value="${user.email}"/>" required> <label for="userEmail">E-mail</label>
	<span id=userEmailError class="invalid-feedback"> <c:out
			value="${form.errors['userEmail']}" />
	</span>
	</div>
<div class="md-form">
	<input type="text" id="userName" name="userName"
		class="form-control <c:choose>
    <c:when test="${form.errors['userName'] == null}">is-valid</c:when>
    <c:otherwise>is-invalid</c:otherwise>
</c:choose>"
		value="<c:out value="${user.name}"/>" required> <label for="userName">Name</label>
	<span id=userNameError class="invalid-feedback"> <c:out
			value="${form.errors['userName']}" />
	</span>
</div>
<div class="md-form">
	<input type="text" id="userFirstname" name="userFirstname"
		class="form-control <c:choose>
    <c:when test="${form.errors['userFirstname'] == null}">is-valid</c:when>
    <c:otherwise>is-invalid</c:otherwise>
</c:choose>"
		value="<c:out value="${user.firstname}"/>"> <label
		for="userFirstname">Firstname</label> <span id=userFirstnameError class="invalid-feedback">
		<c:out value="${form.errors['userFirstname']}" />
	</span>
</div>
<div class="md-form">
	<input id="userPassword" name="userPassword"
		class="form-control <c:choose>
    <c:when test="${form.errors['userPassword'] == null}">is-valid</c:when>
    <c:otherwise>is-invalid</c:otherwise>
</c:choose>"
		type="password" required> <label for="userPassword">Password</label> <span id=userPasswordError
		class="invalid-feedback"> <c:out
			value="${form.errors['userPassword']}" />
	</span>
</div>
<div class="md-form">
	<input id="userConfirm" name="userConfirm"
		class="form-control <c:choose>
    <c:when test="${form.errors['userPassword'] == null}">is-valid</c:when>
    <c:otherwise>is-invalid</c:otherwise>
</c:choose>"
		type="password" required> <label for="userConfirm">Confirm
		Password</label> <span id=userConfirmError class="invalid-feedback"> <c:out
			value="${form.errors['userConfirm']}" />
	</span>
</div>
<c:if test="${ sessionScope.userSession.role == 'SUPER_ADMIN' }">
	<div class="custom-control custom-radio">
		<input type="radio" class="custom-control-input" id="roleUser"
			name="userRole" value="User" checked> <label
			class="custom-control-label" for="roleUser">User</label>
	</div>
	<div class="custom-control custom-radio">
		<input type="radio" class="custom-control-input" id="roleAdmin"
			name="userRole" value="Admin"> <label
			class="custom-control-label" for="roleAdmin">Admin</label>
	</div>
</c:if>