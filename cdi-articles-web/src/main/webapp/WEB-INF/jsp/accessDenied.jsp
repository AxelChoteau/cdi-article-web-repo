<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>CDI Axel Choteau</title>
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/mdb.min.css">
<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<div class="vertical-center">
		<div class="fixed-top">
			<c:import url="inc/header.jsp" />
		</div>
		<div class="container">
			<div class="row">
				<div class="col-12 text-center">
					<h1>Access Denied</h1>
					<c:choose>
						<c:when test="${ empty userSession }">
							<h3>
								You first have to <a href="connection">log in</a> to access this
								page
							</h3>
						</c:when>
						<c:otherwise>
							<h3>You do not have the rights to access this page</h3>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
	</div>
	<script src="js/jquery-3.3.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/mdb.min.js"></script>
</body>
</html>