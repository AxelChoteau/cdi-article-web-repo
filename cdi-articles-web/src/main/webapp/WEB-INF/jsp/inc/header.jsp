<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<nav
	class="navbar navbar-expand-lg navbar-dark grey darken-1 .scrolling-navbar">
	<div class="container">
		<!-- Navbar brand -->
		<a class="navbar-brand" href="/cdi-articles-web"><i
			class="material-icons mr-2  align-middle">shopping_cart</i>Stock Options</a>

		<!-- Collapse button -->
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#basicExampleNav" aria-controls="basicExampleNav"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<!-- Collapsible content -->
		<div class="collapse navbar-collapse" id="basicExampleNav">

			<!-- Links -->
			<ul class="navbar-nav mr-auto">
				<c:if test="${ !empty sessionScope.userSession }">
					<li class="nav-item"><a class="nav-link"
						href="<c:url value="/articles"/>">Articles</a></li>
					<c:if test="${ sessionScope.userSession.role != 'USER' }">
						<li class="nav-item"><a class="nav-link"
							href="<c:url value="/users"/>">Users</a></li>
					</c:if>
				</c:if>
			</ul>
			<!-- Links -->
			<div class="nav-flex-icons">
				<c:choose>
					<c:when test="${ empty sessionScope.userSession }">
						<a
							class="btn btn-sm btn-outline-white btn-rounded nav-link text-white"
							href="/cdi-articles-web/login">Log in<i
							class="fas fa-sign-in-alt ml-2"></i>
						</a>
					</c:when>
					<c:otherwise>
						<a
							class="btn btn-sm btn-outline-white btn-rounded nav-link text-white"
							href="/cdi-articles-web/logout">Log out<i
							class="fas fa-sign-out-alt ml-2"></i>
						</a>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
		<!-- Collapsible content -->
	</div>
</nav>