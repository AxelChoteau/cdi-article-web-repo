<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>CDI Axel Choteau</title>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.6.1/css/all.css">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/mdb.min.css">
<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<div class="vertical-center">
		<div class="fixed-top">
			<c:import url="inc/header.jsp" />
		</div>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12 text-center">
					<!-- 					<i class="fas fa-shopping-cart fa-10x mb-3"></i> -->
					<h1>Welcome to Stock Options</h1>
					<c:choose>
						<c:when test="${ empty sessionScope.userSession }">
							<h4>
								You first have to <a href="login">Login</a> to be able to use
								the application
							</h4>
						</c:when>
						<c:otherwise>
							<h4>
								You are logged in as "
								<c:out value="${ sessionScope.userSession.firstname }" />
								<c:out value="${ sessionScope.userSession.name }" />
								"
							</h4>
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		</div>
	</div>
	<script src="js/jquery-3.3.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/mdb.min.js"></script>
</body>
</html>