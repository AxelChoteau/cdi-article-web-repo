package com.achoteau.cdiarticlesweb.servlets.users;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.achoteau.cdiarticlesweb.dto.UserDto;
import com.achoteau.cdiarticlesweb.forms.AbstractForm;
import com.achoteau.cdiarticlesweb.forms.UserEditForm;
import com.achoteau.cdiarticlesweb.services.IUserService;
import com.achoteau.cdiarticlesweb.services.UserService;

@WebServlet("/users/edit")
public class EditUser extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7039170318258598104L;

	private static final String VUE_FORM = "/WEB-INF/jsp/user/edit.jsp";
	private static final String URL_REDIRECT = "/cdi-articles-web/users";

	private static final String PARAM_USER_ID = "userId";
	private static final String ATT_USER = "user";
	private static final String ATT_FORM = "form";

	private IUserService userService;

	private ServletContext sc;

	@Override
	public void init(ServletConfig config) throws ServletException {
		sc = config.getServletContext();
		WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(sc);
		userService = webApplicationContext.getBean(UserService.class);
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String userId = (String) req.getParameter(PARAM_USER_ID);
		if (userId == null) {
			resp.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		}
		Long id = null;
		try {
			id = Long.valueOf(userId);
		} catch (NumberFormatException e) {
			resp.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		}
		UserDto user = userService.get(id);
		if (user == null) {
			resp.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		}
		req.setAttribute(ATT_USER, user);
		sc.getRequestDispatcher(VUE_FORM).forward(req, resp);

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		AbstractForm<UserDto> form = new UserEditForm(userService);
		UserDto user = form.build(req);
		req.setAttribute(ATT_USER, user);
		req.setAttribute(ATT_FORM, form);
		if (form.getErrors().isEmpty()) {
			userService.update(user);
			resp.sendRedirect(URL_REDIRECT);
		} else {
			sc.getRequestDispatcher(VUE_FORM).forward(req, resp);
		}
	}
}
