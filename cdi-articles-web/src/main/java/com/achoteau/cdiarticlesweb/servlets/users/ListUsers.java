package com.achoteau.cdiarticlesweb.servlets.users;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.achoteau.cdiarticlesweb.dto.UserDto;
import com.achoteau.cdiarticlesweb.services.IUserService;
import com.achoteau.cdiarticlesweb.services.UserService;

@WebServlet("/users")
public class ListUsers extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4104897807143574396L;

	public static final String CONF_USER_REPOSITORY = "userRepository";

	private static final String VUE = "/WEB-INF/jsp/user/list.jsp";

	private static final String ATT_PAGE = "page";
	public static final String ATT_USERS = "users";

	private IUserService userService;

	private ServletContext sc;

	@Override
	public void init(ServletConfig config) throws ServletException {
		sc = config.getServletContext();
		WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(sc);
		userService = webApplicationContext.getBean(UserService.class);
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		int page = 0;
		String pageAtt = (String) req.getParameter(ATT_PAGE);
		if (pageAtt != null && !pageAtt.trim().isEmpty() && pageAtt.matches("\\d")) {
			page = Integer.valueOf(pageAtt);
		}
		Pageable pageable = PageRequest.of(page, 5);
		Page<UserDto> users = userService.getPageUser(pageable);
		req.setAttribute(ATT_USERS, users);
		sc.getRequestDispatcher(VUE).forward(req, resp);
	}
}
