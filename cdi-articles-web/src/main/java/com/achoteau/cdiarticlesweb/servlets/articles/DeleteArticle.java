package com.achoteau.cdiarticlesweb.servlets.articles;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.achoteau.cdiarticlesweb.dto.ArticleDto;
import com.achoteau.cdiarticlesweb.dto.UserDto;
import com.achoteau.cdiarticlesweb.services.ArticleService;
import com.achoteau.cdiarticlesweb.services.IArticleService;

@WebServlet("/articles/details/delete")
public class DeleteArticle extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6455806317937272522L;

	private static final String ATT_ARTICLE_ID = "id";
	public static final String ATT_USER_SESSION = "userSession";

	public static final String URL_REDIRECTION = "/cdi-articles-web/articles";

	private IArticleService articleService;

	private ServletContext sc;

	@Override
	public void init(ServletConfig config) throws ServletException {
		sc = config.getServletContext();
		WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(sc);
		articleService = webApplicationContext.getBean(ArticleService.class);
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		UserDto userSession = (UserDto) session.getAttribute(ATT_USER_SESSION);
		Long articleId = Long.valueOf(req.getParameter(ATT_ARTICLE_ID));
		ArticleDto articleToDelete = articleService.get(articleId);
		if (articleToDelete != null && userSession != null
				&& (userSession.getRole().toString() == "ADMIN" || userSession.getRole().toString() == "SUPER_ADMIN")) {
			articleService.delete(articleToDelete);
		}
		resp.sendRedirect(URL_REDIRECTION);
	}
}
