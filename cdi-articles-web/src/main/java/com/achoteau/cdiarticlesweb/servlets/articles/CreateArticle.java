package com.achoteau.cdiarticlesweb.servlets.articles;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.achoteau.cdiarticlesweb.dto.ArticleDto;
import com.achoteau.cdiarticlesweb.forms.AbstractForm;
import com.achoteau.cdiarticlesweb.forms.ArticleForm;
import com.achoteau.cdiarticlesweb.services.ArticleService;
import com.achoteau.cdiarticlesweb.services.IArticleService;

@WebServlet(urlPatterns = "/articles/create", initParams = @WebInitParam(name = CreateArticle.PATH, value = CreateArticle.IMAGE_STORING_DIRECTORY
		+ "/"))
@MultipartConfig(location = "c:" + CreateArticle.IMAGE_STORING_DIRECTORY, maxFileSize = 2 * 1024
		* 1024, maxRequestSize = 5 * 2 * 1024 * 1024, fileSizeThreshold = 1024 * 1024)
public class CreateArticle extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2113512319000247416L;

	public static final String IMAGE_STORING_DIRECTORY = "/cdi-articles-web/achoteau/fichiers/images";
	public static final String PATH = "path";
	public static final String ATT_ARTICLE = "article";
	public static final String ATT_FORM = "form";

	private static final String VUE_FORM = "/WEB-INF/jsp/article/create.jsp";
	private static final String URL_REDIRECT = "/cdi-articles-web/articles";

	private IArticleService articleService;

	private ServletContext sc;

	private ServletConfig config;

	@Override
	public void init(ServletConfig config) throws ServletException {
		this.config = config;
		sc = config.getServletContext();
		WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(sc);
		articleService = webApplicationContext.getBean(ArticleService.class);
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		sc.getRequestDispatcher(VUE_FORM).forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String path = config.getInitParameter(PATH);
		AbstractForm<ArticleDto> form = new ArticleForm(path);
		ArticleDto article = form.build(req);
		req.setAttribute(ATT_ARTICLE, article);
		req.setAttribute(ATT_FORM, form);
		if (form.getErrors().isEmpty()) {
			articleService.create(article);
			resp.sendRedirect(URL_REDIRECT);
		} else {
			sc.getRequestDispatcher(VUE_FORM).forward(req, resp);
		}
	}

}
