package com.achoteau.cdiarticlesweb.servlets.articles;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLDecoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/articles/details/images/*", initParams = @WebInitParam(name = CreateArticle.PATH, value = CreateArticle.IMAGE_STORING_DIRECTORY
		+ "/"))
public class ImageArticle extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8961582045105898321L;

	public static final int TAILLE_TAMPON = 10240; // 10ko

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		/*
		 * Lecture du paramètre 'chemin' passé à la servlet via la déclaration dans le
		 * web.xml
		 */
		String path = this.getServletConfig().getInitParameter(CreateArticle.PATH);
		/*
		 * Récupération du chemin du fichier demandé au sein de l'URL de la requête
		 */
		String neededFile = req.getPathInfo();

		/* Vérifie qu'un fichier a bien été fourni */
		if (neededFile == null || "/".equals(neededFile)) {
			/*
			 * Si non, alors on envoie une erreur 404, qui signifie que la ressource
			 * demandée n'existe pas
			 */
			resp.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		}

		/*
		 * Décode le nom de fichier récupéré, susceptible de contenir des espaces et
		 * autres caractères spéciaux, et prépare l'objet File
		 */
		neededFile = URLDecoder.decode(neededFile, "UTF-8");
		File file = new File(path, neededFile);

		/* Vérifie que le fichier existe bien */
		if (!file.exists()) {
			/*
			 * Si non, alors on envoie une erreur 404, qui signifie que la ressource
			 * demandée n'existe pas
			 */
			resp.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		}

		/* Récupère le type du fichier */
		String type = getServletContext().getMimeType(file.getName());

		/*
		 * Si le type de fichier est inconnu, alors on initialise un type par défaut
		 */
		if (type == null) {
			type = "application/octet-stream";
		}

		/* Initialise la réponse HTTP */
		resp.reset();
		resp.setBufferSize(TAILLE_TAMPON);
		resp.setContentType(type);
		resp.setHeader("Content-Length", String.valueOf(file.length()));
		resp.setHeader("Content-Disposition", "inline; filename=\"" + file.getName() + "\"");

		try (BufferedInputStream input = new BufferedInputStream(new FileInputStream(file), TAILLE_TAMPON);
				BufferedOutputStream output = new BufferedOutputStream(resp.getOutputStream(), TAILLE_TAMPON);) {

			/* Lit le fichier et écrit son contenu dans la réponse HTTP */
			byte[] tampon = new byte[TAILLE_TAMPON];
			int longueur;
			while ((longueur = input.read(tampon)) > 0) {
				output.write(tampon, 0, longueur);
			}
		}
	}
}
