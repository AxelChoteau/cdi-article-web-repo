package com.achoteau.cdiarticlesweb.servlets;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.achoteau.cdiarticlesweb.services.IUserService;
import com.achoteau.cdiarticlesweb.services.UserService;

@WebServlet("/users/emailCheck")
@Controller
public class EmailCheck extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8172114470762992738L;

	public static final String CONF_USER_REPOSITORY = "userRepository";

	public static final String SUCCESS = "true";
	public static final String FAILURE = "false";

	private IUserService userService;

	private ServletContext sc;

	@Override
	public void init(ServletConfig config) throws ServletException {
		sc = config.getServletContext();
		WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(sc);
		userService = webApplicationContext.getBean(UserService.class);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if (userService.existsByEmail(req.getParameter("email"))) {
			resp.getWriter().write(FAILURE);
		} else {
			resp.getWriter().write(SUCCESS);
		}
	}
}
