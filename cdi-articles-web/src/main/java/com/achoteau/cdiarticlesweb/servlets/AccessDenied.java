package com.achoteau.cdiarticlesweb.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/access-denied")
public class AccessDenied extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4616225959936805433L;

	private static final String VUE = "/WEB-INF/jsp/accessDenied.jsp";

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher(VUE).forward(req, resp);
	}

}
