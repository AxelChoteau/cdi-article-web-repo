package com.achoteau.cdiarticlesweb.servlets;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.achoteau.cdiarticlesweb.dto.UserDto;
import com.achoteau.cdiarticlesweb.forms.AbstractForm;
import com.achoteau.cdiarticlesweb.forms.ConnectionForm;
import com.achoteau.cdiarticlesweb.services.IUserService;
import com.achoteau.cdiarticlesweb.services.UserService;

@WebServlet("/login")
public class Login extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5655603913106472844L;

	public static final String ATT_USER_SESSION = "userSession";
	public static final String ATT_MESSAGE = "message";

	private static final String VUE_FORM = "/WEB-INF/jsp/connection.jsp";
	private static final String VUE_SUCCESS = "/cdi-articles-web";

	private IUserService userService;

	private ServletContext sc;

	@Override
	public void init(ServletConfig config) throws ServletException {
		sc = config.getServletContext();
		WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(sc);
		userService = webApplicationContext.getBean(UserService.class);
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		sc.getRequestDispatcher(VUE_FORM).forward(req, resp);

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		AbstractForm<UserDto> form = new ConnectionForm(userService);
		UserDto user = form.build(req);
		HttpSession session = req.getSession();
		if (user != null) {
			session.setAttribute(ATT_USER_SESSION, user);
			req.setAttribute(ATT_MESSAGE, "You are logged in");
			resp.sendRedirect(VUE_SUCCESS);
		} else {
			req.setAttribute(ATT_MESSAGE, "Connection failed, E-mail or Password are incorrect");
			sc.getRequestDispatcher(VUE_FORM).forward(req, resp);
		}
	}
}
