package com.achoteau.cdiarticlesweb.servlets.articles;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.achoteau.cdiarticlesweb.dto.ArticleDto;
import com.achoteau.cdiarticlesweb.services.ArticleService;
import com.achoteau.cdiarticlesweb.services.IArticleService;

@WebServlet("/articles/details")
public class DetailsArticle extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5400585968472762131L;

	public static final String ATT_ARTICLE = "article";
	public static final String PARAM_ARTICLE_ID = "id";

	private static final String VUE = "/WEB-INF/jsp/article/details.jsp";

	private IArticleService articleService;

	private ServletContext sc;

	@Override
	public void init(ServletConfig config) throws ServletException {
		sc = config.getServletContext();
		WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(sc);
		articleService = webApplicationContext.getBean(ArticleService.class);
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String articleId = (String) req.getParameter(PARAM_ARTICLE_ID);
		if (articleId == null) {
			resp.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		}
		Long id = null;
		try {
			id = Long.valueOf(articleId);
		} catch (NumberFormatException e) {
			resp.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		}
		ArticleDto article = articleService.get(id);
		if (article == null) {
			resp.sendError(HttpServletResponse.SC_NOT_FOUND);
			return;
		}
		req.setAttribute(ATT_ARTICLE, article);
		sc.getRequestDispatcher(VUE).forward(req, resp);
	}
}
