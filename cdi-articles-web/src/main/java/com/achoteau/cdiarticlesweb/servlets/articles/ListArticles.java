package com.achoteau.cdiarticlesweb.servlets.articles;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.achoteau.cdiarticlesweb.dto.ArticleDto;
import com.achoteau.cdiarticlesweb.services.ArticleService;
import com.achoteau.cdiarticlesweb.services.IArticleService;

@WebServlet("/articles")
public class ListArticles extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5739227597926331192L;

	private static final String VUE = "/WEB-INF/jsp/article/list.jsp";

	private static final String ATT_PAGE = "page";
	public static final String ATT_ARTICLES = "articles";

	private IArticleService articleService;

	private ServletContext sc;

	@Override
	public void init(ServletConfig config) throws ServletException {
		sc = config.getServletContext();
		WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(sc);
		articleService = webApplicationContext.getBean(ArticleService.class);
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		int page = 0;
		String pageAtt = (String) req.getParameter(ATT_PAGE);
		if (pageAtt != null && !pageAtt.trim().isEmpty() && pageAtt.matches("\\d")) {
			page = Integer.valueOf(pageAtt);
		}
		Pageable pageable = PageRequest.of(page, 5);
		Page<ArticleDto> articles = articleService.getPageArticle(pageable);
		req.setAttribute(ATT_ARTICLES, articles);
		sc.getRequestDispatcher(VUE).forward(req, resp);
	}

}
