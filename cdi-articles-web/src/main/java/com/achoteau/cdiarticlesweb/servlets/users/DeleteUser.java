package com.achoteau.cdiarticlesweb.servlets.users;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.achoteau.cdiarticlesweb.dto.UserDto;
import com.achoteau.cdiarticlesweb.services.IUserService;
import com.achoteau.cdiarticlesweb.services.UserService;

@WebServlet("/users/delete")
public class DeleteUser extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3775150177137528318L;

	public static final String CONF_USER_REPOSITORY = "userRepository";

	private static final String ATT_USER_ID = "userId";
	public static final String ATT_USER_SESSION = "userSession";

	public static final String URL_REDIRECTION = "/cdi-articles-web/users";

	private IUserService userService;

	private ServletContext sc;

	@Override
	public void init(ServletConfig config) throws ServletException {
		sc = config.getServletContext();
		WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(sc);
		userService = webApplicationContext.getBean(UserService.class);
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		HttpSession session = req.getSession();
		UserDto userSession = (UserDto) session.getAttribute(ATT_USER_SESSION);
		Long userId = Long.valueOf(req.getParameter(ATT_USER_ID));
		UserDto userToDelete = userService.get(userId);
		if (userToDelete != null && userSession != null
				&& ((userToDelete.getRole().toString() == "USER" && userSession.getRole().toString() != "USER")
						|| (userToDelete.getRole().toString() == "ADMIN"
								&& userSession.getRole().toString() == "SUPER_ADMIN"))) {
			userService.delete(userToDelete);
		}
		resp.sendRedirect(URL_REDIRECTION);
	}
}
