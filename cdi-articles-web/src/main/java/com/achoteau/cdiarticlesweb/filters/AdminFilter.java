package com.achoteau.cdiarticlesweb.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.achoteau.cdiarticlesweb.dto.UserDto;
import com.achoteau.cdiarticlesweb.entities.Role;

@WebFilter(urlPatterns = { "/users/*", "/articles/details/edit", "/articles/create", "/articles/details/delete" })
public class AdminFilter implements Filter {

	public static final String ATT_SESSION_USER = "userSession";
	public static final String URL_REDIRECT = "/cdi-articles-web/access-denied";

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;

		HttpSession session = req.getSession();

		UserDto userSession = (UserDto) session.getAttribute(ATT_SESSION_USER);

		if (userSession != null && (userSession.getRole() == Role.ADMIN || userSession.getRole() == Role.SUPER_ADMIN)) {
			chain.doFilter(request, response);
		} else {
			resp.sendRedirect(URL_REDIRECT);
		}
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void destroy() {
	}

}
