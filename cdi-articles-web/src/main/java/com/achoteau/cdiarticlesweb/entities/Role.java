package com.achoteau.cdiarticlesweb.entities;

public enum Role {
	ADMIN, SUPER_ADMIN, USER;
}
