package com.achoteau.cdiarticlesweb.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
@SequenceGenerator(name = "article_generator", initialValue = 1, allocationSize = 1)
public class ArticleEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "article_generator")
	private Long id;
	private String label;
	private String description;
	private Double price;
	private Integer stock;
	private String image;

}
