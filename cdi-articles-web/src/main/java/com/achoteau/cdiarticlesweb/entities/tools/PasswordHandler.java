package com.achoteau.cdiarticlesweb.entities.tools;

import org.jasypt.util.password.ConfigurablePasswordEncryptor;

public class PasswordHandler {

	private static final String ALGO_CHIFFREMENT = "SHA-256";

	private static PasswordHandler passwordHandler;
	ConfigurablePasswordEncryptor passwordEncryptor;

	private PasswordHandler() {
		passwordEncryptor = new ConfigurablePasswordEncryptor();
		passwordEncryptor.setAlgorithm(ALGO_CHIFFREMENT);
		passwordEncryptor.setPlainDigest(false);
	}

	public static PasswordHandler getInstance() {
		if (passwordHandler == null) {
			passwordHandler = new PasswordHandler();
		}
		return passwordHandler;
	}

	public String encryptPassword(String password) {
		return passwordEncryptor.encryptPassword(password);
	}

	public boolean checkPassword(String inputPassword, String encryptedPassword) {
		return passwordEncryptor.checkPassword(inputPassword, encryptedPassword);
	}
}
