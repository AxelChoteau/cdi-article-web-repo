package com.achoteau.cdiarticlesweb.listeners;

import java.io.File;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.achoteau.cdiarticlesweb.dto.UserDto;
import com.achoteau.cdiarticlesweb.entities.Role;
import com.achoteau.cdiarticlesweb.entities.tools.PasswordHandler;
import com.achoteau.cdiarticlesweb.services.IUserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
@EnableJpaRepositories("com.achoteau.cdiarticlesweb.dao")
public class InitializeBDD implements ServletContextListener {

	private static final String IMAGE_STORING_DIRECTORY = "c:/cdi-articles-web/achoteau/fichiers/images";

	@Autowired
	private IUserService userService;

	@Override
	public void contextInitialized(ServletContextEvent event) {

		/* Création du fichier de sauvegarde pour les images */
		File file = new File(IMAGE_STORING_DIRECTORY);
		log.info("Image storing directory path : {}", IMAGE_STORING_DIRECTORY);
		log.info("Image storing directory created : {}", file.mkdirs());

		/* Création dans la base de donnée du SUPER_ADMIN */
		log.info("Creation of SUPER_ADMIN start");
		userService.create(createSuperAdmin());
		log.info("Creation of SUPER_ADMIN end");

	}

	private UserDto createSuperAdmin() {
		return UserDto.builder().name("admin").firstname("super").email("admin@admin.fr")
				.password(PasswordHandler.getInstance().encryptPassword("admin")).role(Role.SUPER_ADMIN).build();
	}

}
