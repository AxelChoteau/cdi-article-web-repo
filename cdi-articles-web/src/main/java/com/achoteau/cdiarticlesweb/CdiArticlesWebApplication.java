package com.achoteau.cdiarticlesweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan
public class CdiArticlesWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(CdiArticlesWebApplication.class, args);
	}

}
