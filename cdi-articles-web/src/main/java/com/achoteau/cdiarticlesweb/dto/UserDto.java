package com.achoteau.cdiarticlesweb.dto;

import com.achoteau.cdiarticlesweb.entities.Role;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;

@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {

	private Long id;
	private String name;
	private String firstname;
	private String email;
	@Exclude
	private String password;
	private Role role;

}
