package com.achoteau.cdiarticlesweb.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ArticleDto {

	private Long id;
	private String label;
	private String description;
	private Double price;
	private Integer stock;
	private String image;

}
