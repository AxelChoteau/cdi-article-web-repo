package com.achoteau.cdiarticlesweb.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.achoteau.cdiarticlesweb.entities.ArticleEntity;

@Repository
public interface ArticleRepository extends PagingAndSortingRepository<ArticleEntity, Long> {

}
