package com.achoteau.cdiarticlesweb.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.achoteau.cdiarticlesweb.dao.ArticleRepository;
import com.achoteau.cdiarticlesweb.dto.ArticleDto;
import com.achoteau.cdiarticlesweb.entities.ArticleEntity;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ArticleService implements IArticleService {

	@Autowired
	ArticleRepository articleRepository;

	@Override
	public Page<ArticleDto> getPageArticle(Pageable pageable) {
		return articleRepository.findAll(pageable).map(articleEntity -> mapToDto(articleEntity));
	}

	@Override
	public ArticleDto get(Long id) {
		ArticleEntity articleEntity = articleRepository.findById(id).orElse(null);
		if (articleEntity == null) {
			return null;
		}
		return mapToDto(articleEntity);
	}

	@Override
	public void create(ArticleDto articleDto) {
		log.info("Article created : {}", mapToDto(articleRepository.save(mapToEntity(articleDto))));
	}

	@Override
	public void update(ArticleDto articleDto) {
		ArticleEntity articleEntity = mapToEntity(articleDto);
		articleEntity.setId(articleDto.getId());
		log.info("User updated : {}", mapToDto(articleRepository.save(articleEntity)));
	}

	@Override
	public void delete(ArticleDto articleDto) {
		ArticleEntity articleEntity = mapToEntity(articleDto);
		articleEntity.setId(articleDto.getId());
		articleRepository.delete(articleEntity);
		log.info("User deleted : {}", articleDto);
	}

	ArticleDto mapToDto(ArticleEntity articleEntity) {
		return ArticleDto.builder().id(articleEntity.getId()).label(articleEntity.getLabel())
				.description(articleEntity.getDescription()).price(articleEntity.getPrice())
				.stock(articleEntity.getStock()).image(articleEntity.getImage()).build();
	}

	ArticleEntity mapToEntity(ArticleDto articleDto) {
		return ArticleEntity.builder().label(articleDto.getLabel()).description(articleDto.getDescription())
				.price(articleDto.getPrice()).stock(articleDto.getStock()).image(articleDto.getImage()).build();
	}

}
