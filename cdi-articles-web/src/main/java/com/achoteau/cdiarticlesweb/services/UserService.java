package com.achoteau.cdiarticlesweb.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.achoteau.cdiarticlesweb.dao.UserRepository;
import com.achoteau.cdiarticlesweb.dto.UserDto;
import com.achoteau.cdiarticlesweb.entities.UserEntity;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UserService implements IUserService {

	@Autowired
	UserRepository userRepository;

	@Override
	public Page<UserDto> getPageUser(Pageable pageable) {
		return userRepository.findAll(pageable).map(userEntity -> mapToDto(userEntity));
	}

	@Override
	public boolean existsByEmail(String email) {
		return userRepository.existsByEmail(email);
	}

	@Override
	public UserDto get(Long id) {
		UserEntity userEntity = userRepository.findById(id).orElse(null);
		if (userEntity == null) {
			return null;
		}
		return mapToDto(userEntity);
	}

	@Override
	public UserDto getByEmail(String email) {
		UserEntity userEntity = userRepository.findByEmail(email).orElse(null);
		if (userEntity == null) {
			return null;
		}
		return mapToDto(userEntity);
	}

	@Override
	public void create(UserDto userDto) {
		log.info("User created : {}", mapToDto(userRepository.save(mapToEntity(userDto))));
	}

	@Override
	public void delete(UserDto userDto) {
		UserEntity userEntity = mapToEntity(userDto);
		userEntity.setId(userDto.getId());
		userRepository.delete(userEntity);
		log.info("User deleted : {}", userDto);
	}

	@Override
	public void update(UserDto userDto) {
		UserEntity userEntity = mapToEntity(userDto);
		userEntity.setId(userDto.getId());
		log.info("User updated : {}", mapToDto(userRepository.save(userEntity)));
	}

	protected UserDto mapToDto(UserEntity userEntity) {
		return UserDto.builder().id(userEntity.getId()).email(userEntity.getEmail()).name(userEntity.getName())
				.firstname(userEntity.getFirstname()).password(userEntity.getPassword()).role(userEntity.getRole())
				.build();
	}

	protected UserEntity mapToEntity(UserDto userDto) {
		return UserEntity.builder().email(userDto.getEmail()).name(userDto.getName()).firstname(userDto.getFirstname())
				.password(userDto.getPassword()).role(userDto.getRole()).build();
	}

}
