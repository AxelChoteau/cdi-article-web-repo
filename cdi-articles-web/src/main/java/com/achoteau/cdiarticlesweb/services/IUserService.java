package com.achoteau.cdiarticlesweb.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.achoteau.cdiarticlesweb.dto.UserDto;

public interface IUserService {

	Page<UserDto> getPageUser(Pageable paegeable);

	boolean existsByEmail(String email);

	UserDto get(Long id);

	UserDto getByEmail(String email);

	void create(UserDto user);

	void delete(UserDto user);

	void update(UserDto user);

}
