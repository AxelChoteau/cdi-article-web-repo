package com.achoteau.cdiarticlesweb.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.achoteau.cdiarticlesweb.dto.ArticleDto;

public interface IArticleService {

	Page<ArticleDto> getPageArticle(Pageable pageable);

	ArticleDto get(Long id);

	void create(ArticleDto articleDto);

	void update(ArticleDto articleDto);

	void delete(ArticleDto articleDto);

}
