package com.achoteau.cdiarticlesweb.forms;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.achoteau.cdiarticlesweb.forms.exceptions.FormValidationException;

import lombok.Getter;

public abstract class AbstractForm<T> {

	@Getter
	protected String result;

	@Getter
	protected Map<String, String> errors = new HashMap<>();

	public abstract T build(HttpServletRequest req);

	/*
	 * Ajoute un message correspondant au champ spécifié à la map des erreurs.
	 */
	protected void setError(String field, String message) {
		errors.put(field, message);
	}

	/*
	 * Méthode utilitaire qui retourne null si un champ est vide, et son contenu
	 * sinon.
	 */
	protected static String getValeurChamp(HttpServletRequest req, String fieldName) {
		String value = req.getParameter(fieldName);
		if (value == null || value.trim().length() == 0) {
			return null;
		} else {
			return value.trim();
		}
	}

	public Long validateId(String id) throws FormValidationException {
		Long processedId = null;
		if (id != null) {
			try {
				processedId = Long.valueOf(id);
				if (processedId < 0) {
					throw new FormValidationException("L'id doit être un nombre entier positif.");
				}
			} catch (NumberFormatException e) {
				throw new FormValidationException("L'id doit être un nombre entier.");
			}
		} else {
			throw new FormValidationException("Merci d'entrer un id.");
		}
		return processedId;
	}

	public Long handleId(String id, String fieldId) {
		Long processedId = null;
		try {
			processedId = validateId(id);
		} catch (FormValidationException e) {
			setError(fieldId, e.getMessage());
		}
		return processedId;
	}
}
