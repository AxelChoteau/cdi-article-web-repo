package com.achoteau.cdiarticlesweb.forms;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.annotation.Configuration;

import com.achoteau.cdiarticlesweb.dto.UserDto;
import com.achoteau.cdiarticlesweb.entities.Role;
import com.achoteau.cdiarticlesweb.entities.tools.PasswordHandler;
import com.achoteau.cdiarticlesweb.forms.exceptions.FormValidationException;
import com.achoteau.cdiarticlesweb.services.IUserService;

@Configuration
public class UserForm extends AbstractForm<UserDto> {

	public static final String FIELD_NAME = "userName";
	public static final String FIELD_FIRSTNAME = "userFirstname";
	public static final String FIELD_EMAIL = "userEmail";
	public static final String FIELD_PASSWORD = "userPassword";
	public static final String FIELD_CONFIRM = "userConfirm";
	public static final String FIELD_ROLE = "userRole";

	private IUserService userService;

	public UserForm(IUserService userService) {
		this.userService = userService;
	}

	@Override
	public UserDto build(HttpServletRequest req) {
		String name = getValeurChamp(req, FIELD_NAME);
		String firstname = getValeurChamp(req, FIELD_FIRSTNAME);
		String email = getValeurChamp(req, FIELD_EMAIL);
		String password = getValeurChamp(req, FIELD_PASSWORD);
		String confirm = getValeurChamp(req, FIELD_CONFIRM);
		String role = getValeurChamp(req, FIELD_ROLE);

		return UserDto.builder().name(handleName(name)).firstname(handleFirstname(firstname)).email(handleEmail(email))
				.password(handlePassword(password, confirm)).role(handleRole(role)).build();
	}

	private void validateName(String name) throws FormValidationException {
		if (name != null) {
			if (name.length() > 255) {
				throw new FormValidationException("Le nom d'utilisateur doit contenir au plus 255 caractères.");
			}
		} else {
			throw new FormValidationException("Merci d'entrer un nom d'utilisateur.");
		}
	}

	private void validateFirstname(String firstname) throws FormValidationException {
		if (firstname != null && firstname.length() > 255) {
			throw new FormValidationException("Le prénom d'utilisateur doit contenir au plus 255 caractères.");
		}
	}

	private void validateEmail(String email) throws FormValidationException {
		if (email != null) {
			if (!email.matches("([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)")) {
				throw new FormValidationException("Merci de saisir une adresse mail valide.");
			}
			if (userService.existsByEmail(email)) {
				throw new FormValidationException("le mail est déjà utilisé");
			}
		} else {
			throw new FormValidationException("Merci de saisir une adresse mail.");
		}
	}

	private void validatePassword(String password, String confirm) throws FormValidationException {
		if (password != null && confirm != null) {
			if (!password.equals(confirm)) {
				throw new FormValidationException(
						"Les mots de passe entrés sont différents, merci de les saisir à nouveau.");
			} else if (password.length() < 5 && password.length() > 255) {
				throw new FormValidationException("Les mots de passe doivent contenir au moins 5 caractères.");
			}
		} else {
			throw new FormValidationException("Merci de saisir et confirmer votre mot de passe.");
		}
	}

	private String handleName(String name) {
		try {
			validateName(name);
		} catch (FormValidationException e) {
			setError(FIELD_NAME, e.getMessage());
		}
		return name;
	}

	private String handleFirstname(String firstname) {
		try {
			validateFirstname(firstname);
		} catch (FormValidationException e) {
			setError(FIELD_FIRSTNAME, e.getMessage());
		}
		return firstname;
	}

	private String handleEmail(String email) {
		try {
			validateEmail(email);
		} catch (FormValidationException e) {
			setError(FIELD_EMAIL, e.getMessage());
		}
		return email;
	}

	/*
	 * Appel à la validation des mots de passe reçus, chiffrement du mot de passe et
	 * initialisation de la propriété motDePasse du bean
	 */
	private String handlePassword(String password, String confirm) {
		try {
			validatePassword(password, confirm);
		} catch (FormValidationException e) {
			setError(FIELD_PASSWORD, e.getMessage());
			setError(FIELD_CONFIRM, null);
		}

		String motDePasseChiffre = PasswordHandler.getInstance().encryptPassword(password);

		return motDePasseChiffre;
	}

	private Role handleRole(String role) {
		if (role == null) {
			return Role.USER;
		}
		switch (role) {
		case "User":
			return Role.USER;
		case "Admin":
			return Role.ADMIN;
		default:
			setError(FIELD_ROLE, "");
			return null;
		}
	}

}
