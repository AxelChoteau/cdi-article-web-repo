package com.achoteau.cdiarticlesweb.forms;

import javax.servlet.http.HttpServletRequest;

import com.achoteau.cdiarticlesweb.dto.UserDto;
import com.achoteau.cdiarticlesweb.entities.Role;
import com.achoteau.cdiarticlesweb.forms.exceptions.FormValidationException;
import com.achoteau.cdiarticlesweb.services.IUserService;

public class UserEditForm extends AbstractForm<UserDto> {

	public static final String FIELD_ID = "userId";
	public static final String FIELD_NAME = "userName";
	public static final String FIELD_FIRSTNAME = "userFirstname";
	public static final String FIELD_ROLE = "userRole";

	private IUserService userService;

	public UserEditForm(IUserService userService) {
		this.userService = userService;
	}

	@Override
	public UserDto build(HttpServletRequest req) {
		String name = getValeurChamp(req, FIELD_NAME);
		String firstname = getValeurChamp(req, FIELD_FIRSTNAME);
		String id = getValeurChamp(req, FIELD_ID);
		String role = getValeurChamp(req, FIELD_ROLE);

		Long processedId = handleId(id, FIELD_ID);
		UserDto userDto = userService.get(processedId);
		if (userDto != null) {
			userDto.setName(handleName(name));
			userDto.setFirstname(handleFirstname(firstname));
			userDto.setRole(handleRole(role));
		}
		return userDto;
	}

	private void validateName(String name) throws FormValidationException {
		if (name != null) {
			if (name.length() > 255) {
				throw new FormValidationException("Le nom d'utilisateur doit contenir au plus 255 caractères.");
			}
		} else {
			throw new FormValidationException("Merci d'entrer un nom d'utilisateur.");
		}
	}

	private void validateFirstname(String firstname) throws FormValidationException {
		if (firstname != null && firstname.length() > 255) {
			throw new FormValidationException("Le prénom d'utilisateur doit contenir au plus 255 caractères.");
		}
	}

	private String handleName(String name) {
		try {
			validateName(name);
		} catch (FormValidationException e) {
			setError(FIELD_NAME, e.getMessage());
		}
		return name;
	}

	private String handleFirstname(String firstname) {
		try {
			validateFirstname(firstname);
		} catch (FormValidationException e) {
			setError(FIELD_FIRSTNAME, e.getMessage());
		}
		return firstname;
	}

	private Role handleRole(String role) {
		if (role == null) {
			return Role.USER;
		}
		switch (role) {
		case "User":
			return Role.USER;
		case "Admin":
			return Role.ADMIN;
		default:
			setError(FIELD_ROLE, "");
			return null;
		}
	}

}
