package com.achoteau.cdiarticlesweb.forms;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import com.achoteau.cdiarticlesweb.dto.UserDto;
import com.achoteau.cdiarticlesweb.entities.tools.PasswordHandler;
import com.achoteau.cdiarticlesweb.services.IUserService;

public class ConnectionForm extends AbstractForm<UserDto> {

	public static final String FIELD_EMAIL = "userEmail";
	public static final String FIELD_PASSWORD = "userPassword";

	@Autowired
	private IUserService userService;

	public ConnectionForm(IUserService userService) {
		this.userService = userService;
	}

	@Override
	public UserDto build(HttpServletRequest req) {
		String email = getValeurChamp(req, FIELD_EMAIL);
		String password = getValeurChamp(req, FIELD_PASSWORD);
		UserDto user = userService.getByEmail(email);
		if (user != null && !PasswordHandler.getInstance().checkPassword(password, user.getPassword())) {
			user = null;
		}
		return user;
	}
}
