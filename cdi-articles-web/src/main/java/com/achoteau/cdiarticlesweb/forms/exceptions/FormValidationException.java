package com.achoteau.cdiarticlesweb.forms.exceptions;

public class FormValidationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -360258614369114579L;

	public FormValidationException(String message) {
		super(message);
	}
}
