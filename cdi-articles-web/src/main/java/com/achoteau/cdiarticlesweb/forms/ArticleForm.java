package com.achoteau.cdiarticlesweb.forms;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import com.achoteau.cdiarticlesweb.dto.ArticleDto;
import com.achoteau.cdiarticlesweb.forms.exceptions.FormValidationException;

import eu.medsea.mimeutil.MimeUtil;

public class ArticleForm extends AbstractForm<ArticleDto> {

	public static final String FIELD_LABEL = "articleLabel";
	public static final String FIELD_DESCRIPTION = "articleDescription";
	public static final String FIELD_PRICE = "articlePrice";
	public static final String FIELD_STOCK = "articleStock";
	public static final String FIELD_IMAGE = "articleImage";

	private static final int TAILLE_TAMPON = 10240; // 10ko

	private String path;

	public ArticleForm(String path) {
		this.path = path;
	}

	@Override
	public ArticleDto build(HttpServletRequest req) {
		String label = getValeurChamp(req, FIELD_LABEL);
		String description = getValeurChamp(req, FIELD_DESCRIPTION);
		String price = getValeurChamp(req, FIELD_PRICE);
		String stock = getValeurChamp(req, FIELD_STOCK);

		ArticleDto article = new ArticleDto();

		handleLabel(label, article);
		handleDescription(description, article);
		handlePrice(price, article);
		handleStock(stock, article);
		handleImage(req, article);

		return article;
	}

	public void validateLabel(String label) throws FormValidationException {
		if (label != null) {
			if (label.length() > 255) {
				throw new FormValidationException("Le nom label doit contenir au plus 255 caractères.");
			}
		} else {
			throw new FormValidationException("Merci d'entrer un label.");
		}
	}

	public void validateDescription(String description) throws FormValidationException {
		if (description != null && description.length() > 255) {
			throw new FormValidationException("La description doit contenir au plus 255 caractères.");
		}
	}

	public Double validatePrice(String price) throws FormValidationException {
		Double processedPrice = null;
		if (price != null) {
			try {
				processedPrice = Double.valueOf(price);
				if (processedPrice < 0) {
					throw new FormValidationException("Le prix doit être un nombre décimal positif.");
				}
			} catch (NumberFormatException e) {
				throw new FormValidationException("Le prix doit être un nombre décimal.");
			}
		} else {
			throw new FormValidationException("Merci d'entrer un prix.");
		}
		return processedPrice;
	}

	public Integer validateStock(String stock) throws FormValidationException {
		Integer processedStock = null;
		if (stock != null) {
			try {
				processedStock = Integer.valueOf(stock);
				if (processedStock < 0) {
					throw new FormValidationException("Le prix doit être un nombre entier positif.");
				}
			} catch (NumberFormatException e) {
				throw new FormValidationException("Le prix doit être un nombre entier.");
			}
		} else {
			throw new FormValidationException("Merci d'entrer un stock.");
		}
		return processedStock;
	}

	public String validateImage(HttpServletRequest req) throws FormValidationException {
		/*
		 * Récupération du contenu du champ image du formulaire. Il faut ici utiliser la
		 * méthode getPart().
		 */
		String fileName = null;
		InputStream fileContent = null;
		try {
			Part part = req.getPart(FIELD_IMAGE);
			fileName = getFileName(part);

			/*
			 * Si la méthode getNomFichier() a renvoyé quelque chose, il s'agit donc d'un
			 * champ de type fichier (input type="file").
			 */
			if (fileName != null && !fileName.isEmpty()) {
				/*
				 * Antibug pour Internet Explorer, qui transmet pour une raison mystique le
				 * chemin du fichier local à la machine du client...
				 * 
				 * Ex : C:/dossier/sous-dossier/fichier.ext
				 * 
				 * On doit donc faire en sorte de ne sélectionner que le nom et l'extension du
				 * fichier, et de se débarrasser du superflu.
				 */
				fileName = fileName.substring(fileName.lastIndexOf('/') + 1).substring(fileName.lastIndexOf('\\') + 1);

				/* Récupération du contenu du fichier */
				fileContent = part.getInputStream();

				/* Extraction du type MIME du fichier depuis l'InputStream */
				MimeUtil.registerMimeDetector("eu.medsea.mimeutil.detector.MagicMimeMimeDetector");
				Collection<?> mimeTypes = MimeUtil.getMimeTypes(fileContent);

				/*
				 * Si le fichier est bien une image, alors son en-tête MIME commence par la
				 * chaîne "image"
				 */
				if (mimeTypes.toString().startsWith("image")) {
					/* Ecriture du fichier sur le disque */
					ecrireFichier(fileContent, fileName);
				} else {
					throw new FormValidationException("Le fichier envoyé doit être une image.");
				}
			} else {
				throw new FormValidationException("Vous devez preciser une image.");
			}
		} catch (IllegalStateException e) {
			/*
			 * Exception retournée si la taille des données dépasse les limites définies
			 * dans la section <multipart-config> de la déclaration de notre servlet
			 * d'upload dans le fichier web.xml
			 */
			e.printStackTrace();
			throw new FormValidationException("Le fichier envoyé ne doit pas dépasser 1Mo.");
		} catch (IOException e) {
			/*
			 * Exception retournée si une erreur au niveau des répertoires de stockage
			 * survient (répertoire inexistant, droits d'accès insuffisants, etc.)
			 */
			e.printStackTrace();
			throw new FormValidationException("Erreur de configuration du serveur.");
		} catch (ServletException e) {
			/*
			 * Exception retournée si la requête n'est pas de type multipart/form-data.
			 */
			e.printStackTrace();
			throw new FormValidationException(
					"Ce type de requête n'est pas supporté, merci d'utiliser le formulaire prévu pour envoyer votre fichier.");
		}

		return fileName;
	}

	public void handleLabel(String label, ArticleDto article) {
		try {
			validateLabel(label);
		} catch (FormValidationException e) {
			setError(FIELD_LABEL, e.getMessage());
		}
		article.setLabel(label);
	}

	public void handleDescription(String description, ArticleDto article) {
		try {
			validateDescription(description);
		} catch (FormValidationException e) {
			setError(FIELD_DESCRIPTION, e.getMessage());
		}
		article.setDescription(description);
	}

	public void handlePrice(String price, ArticleDto article) {
		Double processedPrice = null;
		try {
			processedPrice = validatePrice(price);
		} catch (FormValidationException e) {
			setError(FIELD_PRICE, e.getMessage());
		}
		article.setPrice(processedPrice);
	}

	public void handleStock(String stock, ArticleDto article) {
		Integer processedStock = null;
		try {
			processedStock = validateStock(stock);
		} catch (FormValidationException e) {
			setError(FIELD_STOCK, e.getMessage());
		}
		article.setStock(processedStock);
	}

	public void handleImage(HttpServletRequest req, ArticleDto article) {
		String image = null;
		try {
			image = validateImage(req);
		} catch (FormValidationException e) {
			e.printStackTrace();
			setError(FIELD_IMAGE, e.getMessage());
		}
		article.setImage(image);
	}

	/*
	 * Méthode utilitaire qui a pour unique but d'analyser l'en-tête
	 * "content-disposition", et de vérifier si le paramètre "filename" y est
	 * présent. Si oui, alors le champ traité est de type File et la méthode
	 * retourne son nom, sinon il s'agit d'un champ de formulaire classique et la
	 * méthode retourne null.
	 */
	private static String getFileName(Part part) {
		/* Boucle sur chacun des paramètres de l'en-tête "content-disposition". */
		for (String contentDisposition : part.getHeader("content-disposition").split(";")) {
			/* Recherche de l'éventuelle présence du paramètre "filename". */
			if (contentDisposition.trim().startsWith("filename")) {
				/*
				 * Si "filename" est présent, alors renvoi de sa valeur, c'est-à-dire du nom de
				 * fichier sans guillemets.
				 */
				return contentDisposition.substring(contentDisposition.indexOf('=') + 1).trim().replace("\"", "");
			}
		}
		/* Et pour terminer, si rien n'a été trouvé... */
		return null;
	}

	/*
	 * Méthode utilitaire qui a pour but d'écrire le fichier passé en paramètre sur
	 * le disque, dans le répertoire donné et avec le nom donné.
	 */
	private void ecrireFichier(InputStream fileContent, String fileName) throws FormValidationException {

		try (BufferedInputStream input = new BufferedInputStream(fileContent, TAILLE_TAMPON);
				BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(new File(path + fileName)),
						TAILLE_TAMPON)) {
			/*
			 * Lit le fichier reçu et écrit son contenu dans un fichier sur le disque.
			 */
			byte[] tampon = new byte[TAILLE_TAMPON];
			int length = 0;
			while ((length = input.read(tampon)) > 0) {
				output.write(tampon, 0, length);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new FormValidationException("Erreur lors de l'écriture du fichier sur le disque.");
		}
	}
}
