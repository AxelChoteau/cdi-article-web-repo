/**
 * 
 */



$(function() {
	$("#articleImage").change(function () {
		console.log($("#articleImage").val());
	    $("#fileuploadurl").val($("#articleImage").val().replace(/C:\\fakepath\\/i, ''));
	});
	$("#create_article_form")
			.validate({
						rules : 
							{
							articleLabel : {
								required : true,
								maxlength : 255
							},
							articleDescription :{
								maxlength : 255
							},
							articlePrice : {
								required : true,
								number : true,
								min : 0,
							},
							articleStock : {
								required : true,
								number : true,
								min : 0,
								step : 1
							},
							articleImage : {
								required : false,
								accept : "image/*"
							}
						},
						messages : {
							articleLabel : "Please enter a label.",
							articlePrice : {
								required : "Please provide a price.",
								number : "Price must be a decimal.",
								min : "Price must be positive."
							},
							articleStock : {
								required : "Please provide the stock.",
								number : "Price must be an integer.",
								min : "Price must be positive.",
								step : "Price must be an integer."
							},
							articleImage	 : {
								accept : "Please provide an image.",
							},
						},
						submitHandler : function(form) {
							form.submit();
						},
						onkeyup : false,
						errorElement: 'span',
						errorPlacement: function(error, element) {
							element.removeClass("is-valid");
							element.addClass("is-invalid");
							switch (element.attr("name")){
							case "articleLabel":
								error.appendTo("#articleLabelError");
								break;
							case "articleDescription":
								error.appendTo("#articleDescriptionError");
								break;
							case "articlePrice":
								error.appendTo("#articlePriceError");
								break;
							case "articleStock":
								error.appendTo("#articleStockError");
								break;
							case "articleImage":
								$("#fileuploadurl").removeClass("valid");
								error.appendTo("#articleImageError");
								$("#articleImageError").show();
								break;
							}
							
					    }
					});

});