/**
 * 
 */

$(function() {
	$("#create_user_form")
			.validate({
						rules : 
							{
							userName : {
								required : true,
								maxlength : 255,
							},
							userEmail : {
								maxlength : 255,
								required : true,
								email : true,
								"remote" : {
									url : 'emailCheck',
									type : 'POST',
									data : {
										email : function() {
											return $(
													'#create_user_form :input[name="userEmail"]')
													.val();
										}
									}
								}
							},
							userPassword : {								
								required : true,
								minlength : 5,
								maxlength : 255,
							},
							userConfirm : {
								required : true,
								equalTo : userPassword,
							}
						},
						messages : {
							userName : "Please enter a name",
							userPassword : {
								required : "Please provide a password",
								minlength : "Your password must be at least 5 characters long"
							},
							userConfirm : {
								required : "You have to confirm the password",
								equalTo : "The passwords are differents"
							},
							userEmail	 : {
								required : "Please enter an email address.",
								email : "Please enter a valid email address.",
								remote : jQuery.validator
										.format("{0} is already taken.")
							},
						},
						submitHandler : function(form) {
							form.submit();
						},
						onkeyup : false,
						errorElement: 'span',
						errorPlacement: function(error, element) {
							element.removeClass("is-valid");
							element.addClass("is-invalid");
							switch (element.attr("name")){
							case "userEmail":
								error.appendTo("#userEmailError");
								break;
							case "userName":
								error.appendTo("#userNameError");
								break;
							case "userFirstname":
								error.appendTo("#userFirstnameError");
								break;
							case "userPassword":
								error.appendTo("#userPasswordError");
								break;
							case "userConfirm":
								error.appendTo("#userConfirmError");
								break;
							}
							
					    }
					});

});